import java.util.Scanner;

public class SearchMin {

    public static void main(String[] args) {
        SearchMin searchMin = new SearchMin();
        System.out.println(searchMin.callMinimumFromConsole());
    }

    public int callMinimumFromConsole() {
        Scanner console = new Scanner(System.in);
        int a;
        int min = Integer.MAX_VALUE;
        while (console.hasNextInt()) {
            int num = console.nextInt();
            if (num >= 0) {
                while (num != 0) {
                    a = num % 10;
                    num = num / 10;
                    if (a < min)
                        min = a;
                }
            } else
                break;
        }
        return min;
    }

}
