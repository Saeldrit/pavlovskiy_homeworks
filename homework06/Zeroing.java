import java.util.Arrays;

public class Zeroing {
    public static void main(String[] args) {
        Zeroing zeroing = new Zeroing();
        Operation operation = new Operation();
        zeroing.addZeroToEndArray
                (operation.converterToArrayFromList
                        (operation.consoleNumToList()));
    }

    public void addZeroToEndArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                for (int k = i + 1; k < array.length - 1; k++) {
                    if (array[k] != 0) {
                        array[i] = array[k];
                        array[k] = 0;
                        break;
                    }
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }
}
