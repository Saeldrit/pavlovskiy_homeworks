import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Operation {
    public static void main(String[] args) {
        Operation operation = new Operation();
        System.out.println(operation.getIndexNumFromArray
                (operation.converterToArrayFromList
                        (operation.consoleNumToList()), 5));
    }

    public List consoleNumToList() {
        List<Integer> integerList = new ArrayList<>();
        Scanner console = new Scanner(System.in);
        while (console.hasNextInt()) {
            int num = console.nextInt();
            if ((num >= 0) && (num < Integer.MAX_VALUE))
                integerList.add(num);
            else
                break;
        }
        return integerList;
    }

    public int[] converterToArrayFromList(List integer) {
        int[] array = new int[integer.size()];
        int i = 0;
        for (var element : integer) {
            array[i] = (int) element;
            i++;
        }
        return array;
    }

    public int getIndexNumFromArray(int[] array, int index) {
        int result = 0;
        for (int i = 0; i < array.length; i++) {
            if (index == array[i]) {
                result = i;
                break;
            } else
                result = -1;
        }
        return result;
    }
}
